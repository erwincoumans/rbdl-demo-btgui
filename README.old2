RBDL Demo - Demos and playground for the Rigid Body Dynamics Library
Copyright (c) 2013 Martin Felis <martin.felis@iwr.uni-heidelberg.de>

Introduction
============

This is a set of real-time demos for the Rigid Body Dynamics Library
(RBDL). It is used as a playground and visualization for RBDL development
and comes with its own customized copy of RBDL (mostly bleeding edge
stuff). Therefore, the examples here may or may not run with the official RBDL. 

Requirements
============

You need Qt ([http://http://qt-project.org/](http://qt-project.org/)) and
CMake ([http://www.cmake.org](http://www.cmake.org)) to compile it.

Downloads
=========

You can get the source of latest version from: [http://rbdl.bitbucket.org/MartinFelis/rbdl-demo/get/default.zip](http://rbdl.bitbucket.org/MartinFelis/rbdl-demo/get/default.zip)

Building
========

rbdl-demo uses CMake to generate Makefiles or project files for various
operating systems and IDEs (see ([http://www.cmake.org](http://www.cmake.org)).
The following commands should compile everything on Linux systems if all
requirements are met:

    mkdir build
    cd build/
    cmake -D CMAKE_BUILD_TYPE=Debug ../ 
    make
