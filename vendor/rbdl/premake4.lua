	project "rbdl-static"

	language "C++"
				
	kind "StaticLib"
		
	includedirs {"include"}

	files {
		"src/rbdl_version.cc",
		"src/rbdl_mathutils.cc",
		"src/rbdl_utils.cc",
		"src/Contacts.cc",
		"src/Dynamics.cc",
		"src/Logging.cc",
		"src/Joint.cc",
		"src/Model.cc",
		"src/Kinematics.cc",
	}
