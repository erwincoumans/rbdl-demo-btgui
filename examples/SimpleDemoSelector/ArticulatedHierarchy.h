#ifndef _ARTICULATED_HIERARCHY_H
#define _ARTICULATED_HIERARCHY_H

#include <SimpleMath/SimpleMath.h>

#include <rbdl/rbdl.h>

#include <vector>
#include <iostream>


#include "CollisionShapes.h"

/// Contains visualization infos of all bodies
struct ArticulatedHierarchy {

	ArticulatedHierarchy();

	std::vector<CollisionSphere> collisionSpheres;
	RigidBodyDynamics::Model model;
	RigidBodyDynamics::ConstraintSet constraintSet;
	CollisionPlane plane;

	double velocityDampingFactor;
	RigidBodyDynamics::Math::VectorNd q, qdot, qddot, tau;

	void draw(struct CommonGraphicsApp* app);
	void drawSpherePlaneContacts(struct CommonGraphicsApp* app);
	unsigned int addSphereBodyAtJoint (unsigned int parent_body, double mass, double radius, const RigidBodyDynamics::Math::Vector3d &joint_location, const RigidBodyDynamics::Joint &joint);

	RigidBodyDynamics::Body createSphereBody (double mass, const CollisionSphere &sphere);

	void step(double dt);
	void simulate(double dt);
	void handleCollisions (double dt);
	void resolveCollisions ();
	void updateConstraintSet();
	void debugContacts();

	void updateSphereTransformations();

	void initGeneralizedCoordinates() {
		q = RigidBodyDynamics::Math::VectorNd::Zero (model.q_size);
		qdot = RigidBodyDynamics::Math::VectorNd::Zero (model.qdot_size);
		qddot = RigidBodyDynamics::Math::VectorNd::Zero (model.qdot_size);
		tau = RigidBodyDynamics::Math::VectorNd::Zero (model.qdot_size);
	}
};

// #define _ARTICULATED_HIERARCHY_H
#endif

