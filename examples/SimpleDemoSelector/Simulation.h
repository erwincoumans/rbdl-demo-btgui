#include <vector>
#include <string>

#include "ArticulatedHierarchy.h"

struct Simulation  {
	Simulation();

	unsigned int activeScenarioId;

	ArticulatedHierarchy hierarchy;
	ArticulatedHierarchy hierarchy2;

	double time_accumulator;
	double time_current;
	double stepsize;

	double defaultRestitution;
	double defaultDamping;

	void selectScenario (unsigned int id) {
		//assert for valid id happens in reset
		activeScenarioId = id;
		reset();
	}

	void setDamping (double value) {
		defaultDamping = value;
		hierarchy.velocityDampingFactor = value;
	}
	void setRestitution (double value);

	void reset();
	void update (double timestep);
	void draw(struct CommonGraphicsApp* app);
};

