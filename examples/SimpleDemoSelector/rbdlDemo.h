#ifndef RBDL_DEMO_H
#define RBDL_DEMO_H

#include "Bullet3AppSupport/BulletDemoInterface.h"
#include "OpenGLWindow/CommonGraphicsApp.h"
#include "Bullet3Common/b3Quaternion.h"


///quick demo showing the right-handed coordinate system and positive rotations around each axis
class rbdlDemo : public BulletDemoInterface
{
	struct rbdlDemoInternalData* m_data;
	enum
	{
		numCubesX = 100,
		numCubesY = 100
	};

    CommonGraphicsApp* m_app;
    float m_x;
    float m_y;
    float m_z;
    int m_scenarioIndex;
public:
    
    rbdlDemo(CommonGraphicsApp* app,int scenarioIndex);

    virtual ~rbdlDemo();

    static BulletDemoInterface*    CreateFunc(CommonGraphicsApp* app, int scenarioIndex)
    {
        return new rbdlDemo(app, scenarioIndex);
    }
    
    virtual void    initPhysics();
    virtual void    exitPhysics();
    virtual void	stepSimulation(float deltaTime);
    virtual void	renderScene();
    virtual void	physicsDebugDraw();
    virtual bool	mouseMoveCallback(float x,float y);
    virtual bool	mouseButtonCallback(int button, int state, float x, float y);
    virtual bool	keyboardCallback(int key, int state);
    
};
#endif //RBDL_DEMO_H

