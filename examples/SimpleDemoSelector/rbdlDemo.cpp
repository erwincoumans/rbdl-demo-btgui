#include "rbdlDemo.h"
#include "OpenGLWindow/CommonRenderInterface.h"
#include "OpenGLWindow/SimpleCamera.h"
#include "Simulation.h"

struct rbdlDemoInternalData
{
	Simulation m_simulation;
};

rbdlDemo::rbdlDemo(CommonGraphicsApp* app, int scenarioIndex)
:m_app(app),
m_x(0),
m_y(0),
m_z(0),
m_scenarioIndex(scenarioIndex)
{
	m_data = new rbdlDemoInternalData;

	//printf("scenarioIndex=%d\n",scenarioIndex);

	m_data->m_simulation.selectScenario (scenarioIndex);

	m_app->setUpAxis(1);
#if 0    
	{
        b3Vector3 extents=b3MakeVector3(100,100,100);
        extents[m_app->getUpAxis()]=1;
             
    int boxId = m_app->registerCubeShape(extents[0],extents[1],extents[2]);
    b3Vector3 pos=b3MakeVector3(0,0,0);
        pos[m_app->getUpAxis()]=-1;
    b3Quaternion orn(0,0,0,1);
    b3Vector4 color=b3MakeVector4(0.9,0.9,0.3,1);
    b3Vector3 scaling=b3MakeVector3(1,1,1);
    m_app->m_renderer->registerGraphicsInstance(boxId,pos,orn,color,scaling);
	}

	{
        int boxId = m_app->registerCubeShape(0.1,0.1,0.1);
           
            
            
        for (int i=-numCubesX/2;i<numCubesX/2;i++)
        {
            for (int j = -numCubesY/2;j<numCubesY/2;j++)
            {
                b3Vector3 pos=b3MakeVector3(i,j,j);
                pos[app->getUpAxis()] = 1;
                b3Quaternion orn(0,0,0,1);
                b3Vector4 color=b3MakeVector4(0.3,0.3,0.3,1);
                b3Vector3 scaling=b3MakeVector3(1,1,1);
                m_app->m_renderer->registerGraphicsInstance(boxId,pos,orn,color,scaling);
            }
        }
    }

	m_app->m_renderer->writeTransforms();
#endif

}
    rbdlDemo::~rbdlDemo()
{
    //m_app->m_renderer->enableBlend(false);
	delete m_data;
}
    
 void    rbdlDemo::initPhysics()
{
}
 void    rbdlDemo::exitPhysics()
{
        
}
void	rbdlDemo::stepSimulation(float deltaTime)
{

	m_data->m_simulation.update (deltaTime);// * doubleSpinBoxSpeed->value());

#if 0
    m_x+=0.01f;
    m_y+=0.01f;
	m_z+=0.01f;

    int index=1;//skip ground
        
    for (int i=-numCubesX/2;i<numCubesX/2;i++)
    {
        for (int j = -numCubesY/2;j<numCubesY/2;j++)
        {
            b3Vector3 pos=b3MakeVector3(i,j,j);
            pos[m_app->getUpAxis()] = 1+1*b3Sin(m_x+i-j);
            float orn[4]={0,0,0,1};
            m_app->m_renderer->writeSingleInstanceTransformToCPU(pos,orn,index++);
        }
    }
    m_app->m_renderer->writeTransforms();
#endif

}
 void	rbdlDemo::renderScene()
{
	
	//prepare camera?
	//quick hack to get camera matrices into OpenGL2 fixed pipeline for simulation.draw()
	m_data->m_simulation.draw(m_app);
	m_app->m_renderer->renderScene();
}

	
 void	rbdlDemo::physicsDebugDraw()
{
      		
}
 bool	rbdlDemo::mouseMoveCallback(float x,float y)
{
	return false;   
}
  bool	rbdlDemo::mouseButtonCallback(int button, int state, float x, float y)
{
    return false;   
}
bool	rbdlDemo::keyboardCallback(int key, int state)
{
    return false;   
}
 
