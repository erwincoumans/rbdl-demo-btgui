#ifndef _COLLISION_SHAPES_H
#define _COLLISION_SHAPES_H

#include <SimpleMath/SimpleMath.h>

#include <iostream>

enum ContactState {
		StateNone = 0,
		StateCollision,
		StateContact
	};

struct CollisionSphere {
	CollisionSphere (unsigned int _body_id, double _radius) :
		body_id (_body_id),
		radius (_radius),
		restitution (0.),
		state (StateNone),
		impactVelocity (0.),
		localContactPointSphere (0.f, 0.f, 0.f),
		globalContactPoint (0.f, 0.f, 0.f),
		graphicsInstanceId(-1),
		position (0.f, 0.f, 0.f),
		color (0.8f, 0.2f, 0.2f),
		orientation (Matrix33f::Identity())
	{
		//mesh = CreateUVSphere (8, 16);

		transform = Matrix44f (
				radius, 0.f, 0.f, 0.f,
				0.f, radius, 0.f, 0.f,
				0.f, 0.f, radius, 0.f,
				0.f, 0.f, 0.f, 1.f);
	}

	Matrix44f getOpenGLMatrix() {
		Matrix44f result = Matrix44f::Identity();

		result.block<3,3>(0,0) = orientation;
		result.block<1,3>(3,0) = position.transpose();

		return transform * result;
	}

	unsigned int body_id;
	double radius;
	double restitution;

	ContactState state;
	double impactVelocity;
	Vector3f localContactPointSphere;
	Vector3f globalContactPoint;
	
	int graphicsInstanceId;
	
	Vector3f position;
	Vector3f color;
	Matrix33f orientation;
	Matrix44f transform;
};

struct CollisionPlane {
	CollisionPlane () :
		normal (0.f, 1.f, 0.f),
		d (0.f),
		m_graphicsInstanceId(-1){
		computeTangentVectors();

	};
	CollisionPlane (const Vector3f &_normal, float _d) :
		normal (_normal),
		d (_d) ,
		m_graphicsInstanceId(-1){
		computeTangentVectors();
	};

	void computeTangentVectors() {
		Vector3f temp_tangent = Vector3f (1.f, 0.f, 0.f);
		if (fabs(temp_tangent.transpose() * normal) > 0.7) {
			temp_tangent = Vector3f (0.f, 1.f, 0.f);
		}

		tangent1 = temp_tangent.cross (normal);
		tangent2 = normal.cross (tangent1);
	}
	double calcDistance (const CollisionSphere &sphere) const;
	void calcClosestPoints (const CollisionSphere &sphere, Vector3f *plane_point, Vector3f *sphere_point) const;
	Matrix44f getOpenGLMatrix() {
		Matrix44f result = Matrix44f::Identity();
		result.block<3,1>(0,0) = tangent2;
		result.block<3,1>(0,1) = normal;
		result.block<3,1>(0,2) = -tangent1;
		return result;
	}

	
	Vector3f normal;
	Vector3f tangent1;
	Vector3f tangent2;
	float d;
	int m_graphicsInstanceId;
};

inline double CollisionPlane::calcDistance (const CollisionSphere &sphere) const {
	Vector3f sphere_point = sphere.position - normal * sphere.radius;
	return normal.transpose() * (sphere_point) - d;
}

inline void CollisionPlane::calcClosestPoints (const CollisionSphere &sphere, Vector3f *plane_point, Vector3f *sphere_point) const {
	assert (plane_point != NULL);
	assert (sphere_point != NULL);

	*sphere_point = sphere.position - normal * sphere.radius;

	float distance = normal.transpose() * (*sphere_point) - d;

	*plane_point = (*sphere_point) - normal * distance;
}

/* _COLLISION_SHAPES_H */
#endif 

