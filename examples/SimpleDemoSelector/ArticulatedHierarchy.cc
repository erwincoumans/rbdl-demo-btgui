#include <SimpleMath/SimpleMath.h>

#include <rbdl/rbdl.h>

#include <vector>
#include <iostream>


#include "ArticulatedHierarchy.h"
#include "CollisionShapes.h"
#include "OpenGLWindow/CommonRenderInterface.h"
#include "OpenGLWindow/CommonGraphicsApp.h"

using namespace std;

const double eps_coll_dist = 1.0e-2;
const double eps_coll_velocity_contact = 0.1;

//using namespace RigidBodyDynamics::Math;
//typedef SimpleMath::Vector3f Vector3f;
//typedef SimpleMath::Matrix33f Matrix33f;

template <typename OutType, typename InType>
OutType ConvertVector(const InType &in_vec) {
	OutType result = OutType::Zero (in_vec.size());
	for (size_t i = 0; i < in_vec.size(); i++) {
		result[i] = in_vec[i];
	}

	return result;
}

template <typename OutType, typename InType>
OutType ConvertMatrix(const InType &in_vec) {
	OutType result = OutType::Zero (in_vec.rows(), in_vec.cols());
	for (size_t i = 0; i < in_vec.rows(); i++) {
		for (size_t j = 0; j < in_vec.cols(); j++) {
			result(i,j) = in_vec(i,j);
		}
	}

	return result;
}

template
Vector3f ConvertVector<Vector3f, Vector3d> (const Vector3d &in_vec);

ArticulatedHierarchy::ArticulatedHierarchy()
{
}

void ArticulatedHierarchy::draw(CommonGraphicsApp* app) {
	updateSphereTransformations();

	

	//static Quaternion fromMatrix (const Matrix3d &mat) {
	if (plane.m_graphicsInstanceId<0)
	{
		float color0[4] = {0.1, 0.1, 0.1,1};
		float color1[4] = {0.6, 0.6, 0.6,1};
		app->registerGrid(20,20,color0,color1);
		plane.m_graphicsInstanceId = 1;
	}
	



	// draw collision spheres
	std::vector<CollisionSphere>::iterator body_iter = collisionSpheres.begin();


	while (body_iter != collisionSpheres.end()) {

		float pos[4] = {body_iter->position[0],body_iter->position[1],body_iter->position[2],1};
		RigidBodyDynamics::Math::Quaternion orn1 = RigidBodyDynamics::Math::Quaternion::fromMatrix (body_iter->orientation);
		float orn[4]={orn1[0],orn1[1],orn1[2],orn1[3]};
		float color[4] = {0.8f, 0.4f, 0.4f,1.f};
		float radius = body_iter->radius;
		float scaling[4] = {radius,radius,radius,1};
		
		if (body_iter->graphicsInstanceId<0)
		{
			int shapeIndex = app->registerGraphicsSphereShape(radius,false);
			body_iter->graphicsInstanceId = app->m_renderer->registerGraphicsInstance(shapeIndex,pos,orn,color,scaling);
		}
		assert(body_iter->graphicsInstanceId>=0);
		app->m_renderer->writeSingleInstanceTransformToCPU(pos,orn,body_iter->graphicsInstanceId);
		app->m_renderer->writeSingleInstanceColorToCPU(color,body_iter->graphicsInstanceId);
		
#if 0
		glPushMatrix();

		Matrix44f body_gl_matrix = body_iter->getOpenGLMatrix();
		glMultMatrixf(body_gl_matrix.data());

		// we draw both polygons and wireframe
		glPushMatrix();
		glScalef (1.001f, 1.001f, 1.001f);
		glColor3f (0.8f, 0.4f, 0.4f);
		glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
		body_iter->mesh.draw (GL_TRIANGLES);
		glPopMatrix();

		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		if (body_iter->state == StateNone)
			glColor3f (0.3, 0.3, 0.8);
		else if  (body_iter->state == StateCollision)
			glColor3f (0.3, 0.8, 0.8);
		else
			glColor3f (0.8, 0.3, 0.3);

		body_iter->mesh.draw (GL_TRIANGLES);

		glPopMatrix();
#endif

		body_iter++;
	}


	// draw connections between the bodies
	unsigned int first_nonvirtual_body_id = 0;
	for (unsigned int i = 1; i < model.mBodies.size(); i++) {
		if (model.mBodies[i].mMass != 0.) {
			first_nonvirtual_body_id = i;
			break;
		}
	}

	if (first_nonvirtual_body_id + 1< model.mBodies.size()) {
		
		std::vector<Vector3f> positions;
		std::vector<unsigned int> indices;
		//float color[4] = {0.9,0.9,0.9,1};
		float color[4] = {0.2,0.2,0.2,1};
		float lineWidth = 5.f;
		bool useBatchedLineDrawing = true;
		for (unsigned int body_id = first_nonvirtual_body_id + 1; body_id < model.mBodies.size(); body_id++) {
			Vector3f body_position = ConvertVector<Vector3f, RigidBodyDynamics::Math::Vector3d> (CalcBodyToBaseCoordinates (model, q, body_id, Vector3d (0., 0., 0.), false));
			Vector3f parent_position = ConvertVector<Vector3f, RigidBodyDynamics::Math::Vector3d> (CalcBodyToBaseCoordinates (model, q, model.m_parentIds[body_id], Vector3d (0., 0., 0.), false));
			if (useBatchedLineDrawing)
			{
				positions.push_back(parent_position);
				positions.push_back(body_position);
				indices.push_back(indices.size());
				indices.push_back(indices.size());
			} else
			{
				app->m_renderer->drawLine(parent_position.data(),body_position.data(),color,lineWidth);
			}
			//connection_lines.addVerticefv (parent_position.data());
			//connection_lines.addColor (0.9, 0.9, 0.9);
			//connection_lines.addVerticefv (body_position.data());
			//connection_lines.addColor (0.9, 0.9, 0.9);
		}
		
		
		if (useBatchedLineDrawing)
		{
			app->m_renderer->drawLines(positions[0].data(),color,positions.size(),sizeof(Vector3f),&indices[0],indices.size(),lineWidth);
		}

	}

	app->m_renderer->writeTransforms();
}

void ArticulatedHierarchy::drawSpherePlaneContacts (CommonGraphicsApp* app) {
	updateSphereTransformations();

	std::vector<CollisionSphere>::iterator body_iter = collisionSpheres.begin();


	while (body_iter != collisionSpheres.end()) {
		// draw closest points
		Vector3f sphere_point;
		Vector3f plane_point;

		plane.calcClosestPoints (*body_iter, &plane_point, &sphere_point);
		float color[4]={1,0,0,1};
		int width=1;
		app->m_renderer->drawLine(plane_point.data(),sphere_point.data(),color,width);
/*
		glColor3f (1.f, 0.f, 0.f);
		glBegin(GL_LINES);
		glVertex3fv (plane_point.data());
		glVertex3fv (sphere_point.data());
		glEnd();
*/
		body_iter++;
	}
}

unsigned int ArticulatedHierarchy::addSphereBodyAtJoint (unsigned int parent_body, double mass, double radius, const Vector3d &joint_location, const RigidBodyDynamics::Joint &joint) {
	RigidBodyDynamics::Body sphere_body (mass, Vector3d (0., 0., 0.), 
			Vector3d (
				2.* mass * radius * radius,
				2.* mass * radius * radius,
				2.* mass * radius * radius)
			);

	unsigned int sphere_body_id = model.AddBody (parent_body, RigidBodyDynamics::Math::Xtrans (joint_location), joint, sphere_body);

	CollisionSphere geometry_sphere (sphere_body_id, radius);
	collisionSpheres.push_back (geometry_sphere);

	return sphere_body_id;
}

void ArticulatedHierarchy::updateConstraintSet() {
	updateSphereTransformations();

	constraintSet = RigidBodyDynamics::ConstraintSet();

	for (unsigned int i = 0; i < collisionSpheres.size(); i++) {
		if (collisionSpheres[i].state != StateContact)
			continue;

		Vector3f sphere_point;
		Vector3f plane_point;
		plane.calcClosestPoints (collisionSpheres[i], &plane_point, &sphere_point);
		Vector3d sphere_point_local = CalcBaseToBodyCoordinates (model, q, collisionSpheres[i].body_id, ConvertVector<Vector3d, Vector3f>(sphere_point));

		constraintSet.AddConstraint (collisionSpheres[i].body_id, Vector3d (0., 0., 0.), ConvertVector<Vector3d, Vector3f>(plane.normal), NULL, 0.);
//		constraintSet.AddConstraint (collisionSpheres[i].body_id, sphere_point_local, plane.normal, NULL, 0.);
		constraintSet.AddConstraint (collisionSpheres[i].body_id, sphere_point_local, ConvertVector<Vector3d, Vector3f>(plane.tangent1));
		constraintSet.AddConstraint (collisionSpheres[i].body_id, sphere_point_local, ConvertVector<Vector3d, Vector3f>(plane.tangent2));
	}

	if (constraintSet.size() > 0) {
		constraintSet.Bind(model);
	}
}

void ArticulatedHierarchy::resolveCollisions () {
	RigidBodyDynamics::ConstraintSet cs;

	for (unsigned int i = 0; i < collisionSpheres.size(); i++) {
		if (collisionSpheres[i].state != StateCollision)
			continue;

		cs.AddConstraint (collisionSpheres[i].body_id, Vector3d(0., 0., 0.), ConvertVector<Vector3d, Vector3f>(plane.normal));

		double resolve_velocity = - collisionSpheres[i].impactVelocity * collisionSpheres[i].restitution;
		if (resolve_velocity < eps_coll_velocity_contact) {
			cout << "Bouncing to contact!" << endl;
			resolve_velocity = 0.;
			collisionSpheres[i].state = StateContact;
		}

		cout << "Resolve velocity = " << resolve_velocity << " " << endl;;

		cs.v_plus[cs.size() - 1] = resolve_velocity;

		Vector3f sphere_point;
		Vector3f plane_point;
		plane.calcClosestPoints (collisionSpheres[i], &plane_point, &sphere_point);
		Vector3d sphere_point_local = CalcBaseToBodyCoordinates (model, q, collisionSpheres[i].body_id, ConvertVector<Vector3d, Vector3f>(sphere_point));

		cs.AddConstraint (collisionSpheres[i].body_id, sphere_point_local, ConvertVector<Vector3d, Vector3f>(plane.tangent1));
		cs.AddConstraint (collisionSpheres[i].body_id, sphere_point_local, ConvertVector<Vector3d, Vector3f>(plane.tangent2));
	}

	cs.Bind (model);

	RigidBodyDynamics::Math::VectorNd qdotplus = qdot;
	ComputeContactImpulsesLagrangian (model, q, qdot, cs, qdotplus);

	qdot = qdotplus;
}

void ArticulatedHierarchy::debugContacts() {
	updateSphereTransformations();

	for (unsigned int i = 0; i < collisionSpheres.size(); i++) {
		if (collisionSpheres[i].state == StateNone) {
			cout << "contactInfo[" << i << "] : none" << endl;
			continue;
		}

		Vector3f sphere_point;
		Vector3f plane_point;
		plane.calcClosestPoints (collisionSpheres[i], &plane_point, &sphere_point);
		double distance = plane.calcDistance(collisionSpheres[i]);

		Vector3d sphere_point_local = CalcBaseToBodyCoordinates (model, q, collisionSpheres[i].body_id, ConvertVector<Vector3d, Vector3f>(sphere_point));
	
		Vector3d sphere_contact_velocity = CalcPointVelocity (model, q, qdot, collisionSpheres[i].body_id, sphere_point_local);
		double impact_velocity = ConvertVector<Vector3d, Vector3f>(plane.normal).transpose() * sphere_contact_velocity;
		
		Vector3d sphere_contact_accel = CalcPointAcceleration (model, q, qdot, qddot, collisionSpheres[i].body_id, sphere_point_local);
		double impact_accel = ConvertVector<Vector3d, Vector3f>(plane.normal).transpose() * sphere_contact_accel;

		cout << "contactInfo[" << i << "] : dist = " << distance 
			<< " impact_vel = " << impact_velocity 
			<<  " impact_acc = " << impact_accel
//			<< " sphere_local = " << sphere_point_local.transpose()
//			<< " sphere_global = " << sphere_point.transpose()
			<< " contact_vel = " << sphere_contact_velocity.transpose()
			<< " contact_acc = " << sphere_contact_accel.transpose() << endl;
	}
}

void ArticulatedHierarchy::handleCollisions (double dt) {
	RigidBodyDynamics::Math::VectorNd q_old (q);
	RigidBodyDynamics::Math::VectorNd qdot_old (qdot);

	step(dt);
	updateSphereTransformations();

	bool found_collisions = false;

	for (unsigned int i = 0; i < collisionSpheres.size(); i++) {
		Vector3f sphere_point;
		Vector3f plane_point;
		plane.calcClosestPoints (collisionSpheres[i], &plane_point, &sphere_point);
		double distance = plane.calcDistance(collisionSpheres[i]);

		Vector3d sphere_point_local = CalcBaseToBodyCoordinates (model, q, collisionSpheres[i].body_id, ConvertVector<Vector3d, Vector3f>(sphere_point));
		Vector3d sphere_contact_velocity = CalcPointVelocity (model, q, qdot, collisionSpheres[i].body_id, sphere_point_local);
		double impact_velocity = ConvertVector<Vector3d, Vector3f>(plane.normal).transpose() * sphere_contact_velocity;

		if (distance > eps_coll_dist && impact_velocity > eps_coll_velocity_contact) {
			if (collisionSpheres[i].state == StateContact) {
				cout << "vanishing: impact velocity = " << impact_velocity << endl;
			}

			collisionSpheres[i].state = StateNone;
		} else if (distance <= eps_coll_dist) {
			if (collisionSpheres[i].state != StateContact && impact_velocity < -eps_coll_velocity_contact) {
				// collision
				found_collisions = true;

				cout << "collision: impact velocity = " << impact_velocity << endl;
//				assert (collisionSpheres[i].state != StateContact);
				collisionSpheres[i].state = StateCollision;
				collisionSpheres[i].impactVelocity = impact_velocity;
				collisionSpheres[i].localContactPointSphere = ConvertVector<Vector3f, Vector3d>(sphere_point_local);

			} else {
				//				cout << "contact: impact velocity = " << impact_velocity << endl;
				collisionSpheres[i].state = StateContact;
			}
		}
	}

	q = q_old;
	qdot = qdot_old;
	updateSphereTransformations();

	if (found_collisions) {
		resolveCollisions();
		debugContacts();
	}
}



void ArticulatedHierarchy::step (double dt) {
	if (model.q_size != model.qdot_size) {
		for (unsigned int i = 1; i < model.mJoints.size(); i++) {
			unsigned int q_index = model.mJoints[i].q_index;
			if (model.mJoints[i].mJointType == RigidBodyDynamics::JointTypeSpherical) {
				RigidBodyDynamics::Math::Quaternion quat = model.GetQuaternion (i, q);

				Vector3d omega (qdot[q_index+0], qdot[q_index + 1], qdot[q_index + 2]);
				if (omega.squaredNorm() > 0.0001) {
					RigidBodyDynamics::Math::Quaternion quat_omega (RigidBodyDynamics::Math::Quaternion::fromAxisAngle (omega.normalized(), dt * omega.norm()));

					quat = quat_omega * quat;

					quat.normalize();
				}

//				cout << "q[" << i << "] = " <<  quat.transpose() << " ||q|| = " << quat.norm() << endl;
				model.SetQuaternion (i, quat, q);
			} else {
				q[q_index] = dt * qdot[q_index];
			}
		}
		qdot = qdot + dt * qddot;
	} else {
		// Euler
		q = q + dt * qdot;
		qdot = qdot + dt * qddot;
	}
}

void ArticulatedHierarchy::simulate (double dt) {
	handleCollisions (dt);
	updateConstraintSet();

	tau = (qdot * velocityDampingFactor) * -1.;

	if (constraintSet.size() > 0) {
		ForwardDynamicsContactsLagrangian (model, q, qdot, tau, constraintSet, qddot);
//		cout << "Contact: ";
//		debugContacts();
	} else {
		ForwardDynamics (model, q, qdot, tau, qddot);
	}

	if (0) {
		// Leapfrog SE1
		VectorNd qdot_2 = qdot + qddot * dt * 0.5;
		VectorNd q_2 = q + qdot_2 * dt * 0.5;

		// Leapfrog SE2
		q = q_2 + qdot_2 * dt * 0.5;

		VectorNd qddot_2 = qddot;
		if (constraintSet.size() > 0) {
			ForwardDynamicsContactsLagrangian (model, q, qdot, tau, constraintSet, qddot_2);
			//		cout << "Contact: ";
			//		debugContacts();
		} else {
			ForwardDynamics (model, q, qdot, tau, qddot_2);
		}

		qdot = qdot_2 + qddot_2 * dt * 0.5;
	} else {
		step (dt);
	}
}

void ArticulatedHierarchy::updateSphereTransformations() {
	std::vector<CollisionSphere>::iterator body_iter = collisionSpheres.begin();

	UpdateKinematicsCustom (model, &q, NULL, NULL);

	while (body_iter != collisionSpheres.end()) {
		body_iter->orientation = ConvertMatrix<Matrix33f, RigidBodyDynamics::Math::Matrix3d>(CalcBodyWorldOrientation (model, q, body_iter->body_id, false));
		body_iter->position = ConvertVector<Vector3f, Vector3d>(CalcBodyToBaseCoordinates (model, q, body_iter->body_id, Vector3d (0., 0., 0.), false));

		body_iter++;
	}
}
