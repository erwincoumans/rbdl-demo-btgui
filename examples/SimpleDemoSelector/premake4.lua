	
		project "App_SimpleDemoSelector"

		language "C++"
				
		kind "ConsoleApp"

  	includedirs {
                ".",
                "../../btgui",
                "../../vendor/rbdl/include",
                "../../vendor/rbdl/include/rbdl",
               	"../rbdl_urdfreader", 
		}

			
		links{"urdf_tinyxml","rbdl-static","Bullet3AppSupport","gwen", "OpenGL_Window","Bullet3Common"}
		initOpenGL()
		initGlew()

    
		files {
		"*.cpp",
		"*.cc",
		"*.h",
		"../rbdl_urdfreader/rbdl_urdfreader.cc",
		"../rbdl_urdfreader/rbdl_urdfreader.h",	
		
		}

if os.is("Linux") then 
	initX11()
end
if os.is("MacOSX") then
	links{"Cocoa.framework"}
end
