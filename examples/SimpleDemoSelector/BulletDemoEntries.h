
#ifndef BULLET_DEMO_ENTRIES_H
#define BULLET_DEMO_ENTRIES_H

#include "Bullet3AppSupport/BulletDemoInterface.h"
#include "CoordinateSystemDemo.h"
#include "RenderInstancingDemo.h"
#include "rbdlDemo.h"




struct DemoEntry
{
	int									m_menuLevel;
	const char*							m_name;
	BulletDemoInterface::CreateFunc*	m_createFunc;
	int									m_scenarioIndex;
};



static DemoEntry allDemoEntries[]=
{

    {0,"Basic Concepts",0},
    {1,"Basis Frame", &CoordinateSystemDemo::CreateFunc},
    {0,"Rendering",0},
    {1,"Instanced Shapes", &RenderInstancingDemo::CreateFunc},
	{0,"RBDL",0},
	{1,"Falling Simple",rbdlDemo::CreateFunc, 0},
	{1,"Falling Linear Velocity",rbdlDemo::CreateFunc, 1},
	{1,"Falling Angular Velocity",rbdlDemo::CreateFunc, 2},
	{1,"Falling Lateral Velocity",rbdlDemo::CreateFunc, 3},
	{1,"Falling Sloped",rbdlDemo::CreateFunc, 4},
	{1,"Pendulum Swing Crash",rbdlDemo::CreateFunc, 5},
	{1,"Pendulum Swing Spherical",rbdlDemo::CreateFunc, 6},
	{1,"Triple Pendulum Comparison",rbdlDemo::CreateFunc, 7},
	{1,"Chain Fixed",rbdlDemo::CreateFunc, 8},
	{1,"Chain Fixed Contact",rbdlDemo::CreateFunc, 9},
	{1,"Chain Falling",rbdlDemo::CreateFunc, 10},

};

#include <stdio.h>


static void saveCurrentDemoEntry(int currentEntry,const char* startFileName)
{
	FILE* f = fopen(startFileName,"w");
	if (f)
	{
		fprintf(f,"%d\n",currentEntry);
		fclose(f);
	}
};

static int loadCurrentDemoEntry(const char* startFileName)
{
	int currentEntry= 0;
	FILE* f = fopen(startFileName,"r");
	if (f)
	{
		int result;
		result = fscanf(f,"%d",&currentEntry);
		if (result)
		{
			return currentEntry;
		}
		fclose(f);
	}
	return 0;
};

#endif//BULLET_DEMO_ENTRIES_H

