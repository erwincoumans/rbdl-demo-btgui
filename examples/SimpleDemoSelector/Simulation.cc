#include <SimpleMath/SimpleMath.h>

#include <rbdl/rbdl.h>
#include <rbdl/rbdl_utils.h>


#include <iostream>


#include "CollisionShapes.h"
#include "ArticulatedHierarchy.h"
#include "Simulation.h"

using namespace std;
using namespace RigidBodyDynamics;
//using namespace RigidBodyDynamics::Math;
using namespace RigidBodyDynamics::Utils;

typedef SimpleMath::Fixed::Matrix<float, 4, 4> Matrix44f;



Joint floating_base (
		RigidBodyDynamics::Math::SpatialVector (0., 0., 0., 1., 0., 0.),
		RigidBodyDynamics::Math::SpatialVector (0., 0., 0., 0., 1., 0.),
		RigidBodyDynamics::Math::SpatialVector (0., 0., 0., 0., 0., 1.),
		RigidBodyDynamics::Math::SpatialVector (0., 0., 1., 0., 0., 0.),
		RigidBodyDynamics::Math::SpatialVector (0., 1., 0., 0., 0., 0.),
		RigidBodyDynamics::Math::SpatialVector (1., 0., 0., 0., 0., 0.));

Joint spherical_zyx (
		RigidBodyDynamics::Math::SpatialVector (0., 0., 1., 0., 0., 0.),
		RigidBodyDynamics::Math::SpatialVector (0., 1., 0., 0., 0., 0.),
		RigidBodyDynamics::Math::SpatialVector (1., 0., 0., 0., 0., 0.));

Joint spherical (
		JointTypeSpherical
		);

Joint rot_zx (
		RigidBodyDynamics::Math::SpatialVector (1., 0., 0., 0., 0., 0.),
		RigidBodyDynamics::Math::SpatialVector (0., 0., 1., 0., 0., 0.));

Joint rot_x ( RigidBodyDynamics::Math::SpatialVector (1., 0., 0., 0., 0., 0.));
Joint rot_y ( RigidBodyDynamics::Math::SpatialVector (0., 1., 0., 0., 0., 0.));
Joint rot_z ( RigidBodyDynamics::Math::SpatialVector (0., 0., 1., 0., 0., 0.));


void init_simple_hierarchy_falling(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	hierarchy.addSphereBodyAtJoint (0, 2., 0.5, Vector3d (0., 0., 0.), floating_base);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.q[1] = 1.6;
}

void init_simple_hierarchy_linear_velocity(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);

	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	hierarchy.addSphereBodyAtJoint (0, 2., 0.5, Vector3d (0., 0., 0.), floating_base);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.q[1] = hierarchy.collisionSpheres[0].radius + 0.5;
	hierarchy.qdot[2] = 3.5;
}

void init_simple_hierarchy_rotating(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	hierarchy.addSphereBodyAtJoint (0, 5., 0.5, Vector3d (0., 0., 0.), floating_base);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.q[1] = 1.6;
	hierarchy.qdot[1] = 3.;
	hierarchy.qdot[2] = -1.;
	hierarchy.qdot[5] = 10.;
}

void init_simple_hierarchy_rotating_lateral(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	Joint floating_base (
			RigidBodyDynamics::Math::SpatialVector (0., 0., 0., 1., 0., 0.),
			RigidBodyDynamics::Math::SpatialVector (0., 0., 0., 0., 1., 0.),
			RigidBodyDynamics::Math::SpatialVector (0., 0., 0., 0., 0., 1.),
			RigidBodyDynamics::Math::SpatialVector (0., 0., 1., 0., 0., 0.),
			RigidBodyDynamics::Math::SpatialVector (0., 1., 0., 0., 0., 0.),
			RigidBodyDynamics::Math::SpatialVector (1., 0., 0., 0., 0., 0.));

	hierarchy.addSphereBodyAtJoint (0, 5., 0.5, Vector3d (0., 0., 0.), floating_base);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.q[1] = 1.6;
	hierarchy.qdot[1] = 3.;
	hierarchy.qdot[2] = -1.;
	hierarchy.qdot[3] = 10.;
}

void init_simple_hierarchy_sloped(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	double angle = 0.2;
	hierarchy.plane = CollisionPlane (Vector3f (0.f, cos(angle), -sin(angle)), 0.f);

	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	hierarchy.addSphereBodyAtJoint (0, 2., 0.5, Vector3d (0., 0., 0.), floating_base);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.q[1] = hierarchy.collisionSpheres[0].radius + 0.5;
	hierarchy.qdot[1] = 3.;
	hierarchy.qdot[2] = -1.;
	hierarchy.qdot[5] = 5.;
}

void init_pendulum_crash(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);

	// initialize model
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	unsigned int sphere1_id = hierarchy.addSphereBodyAtJoint (0, 0.3, 0.15, Vector3d (0., 3., 0.), spherical_zyx);
	unsigned int sphere2_id = hierarchy.addSphereBodyAtJoint (sphere1_id, 0.3, 0.15, Vector3d (0., -1.1, 0.), spherical_zyx);
	unsigned int sphere3_id = hierarchy.addSphereBodyAtJoint (sphere2_id, 0.3, 0.15, Vector3d (0., -1.1, 0.), spherical_zyx );

	hierarchy.initGeneralizedCoordinates();

	hierarchy.q[0] = 0.4;

	hierarchy.q[3] = 0.4;
	hierarchy.q[4] = 0.4;
	hierarchy.q[5] = 0.4;

	hierarchy.qdot[1] = 1.;
}

void init_pendulum_spherical(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);

	// initialize model
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	Joint joint_spherical (JointTypeSpherical);

	unsigned int sphere1_id = hierarchy.addSphereBodyAtJoint (0, 0.3, 0.15, Vector3d (0., 3., 0.), joint_spherical);
	unsigned int sphere2_id = hierarchy.addSphereBodyAtJoint (sphere1_id, 0.3, 0.15, Vector3d (0., -1.1, 0.), joint_spherical);
	unsigned int sphere3_id = hierarchy.addSphereBodyAtJoint (sphere2_id, 0.3, 0.15, Vector3d (0., -1.1, 0.), joint_spherical);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.model.SetQuaternion (1, Math::Quaternion::fromZYXAngles (Vector3d (0., 0., 0.)), hierarchy.q);
	hierarchy.model.SetQuaternion (2, Math::Quaternion::fromZYXAngles (Vector3d (0., 0., 0.)), hierarchy.q);
	hierarchy.model.SetQuaternion (3, Math::Quaternion::fromZYXAngles (Vector3d (0., 0., 0.)), hierarchy.q);

	hierarchy.model.SetQuaternion (1, Math::Quaternion::fromZYXAngles (Vector3d (0.4, 0., 0.)), hierarchy.q);
	hierarchy.model.SetQuaternion (2, Math::Quaternion::fromZYXAngles (Vector3d (0.4, 0.4, 0.4)), hierarchy.q);

	Vector3d omega1 = RigidBodyDynamics::Math::angular_velocity_from_angle_rates (
						Vector3d (0.4, 0., 0.),
						Vector3d (0., 1., 0.)
						);

	hierarchy.qdot[0] = omega1[2];
	hierarchy.qdot[1] = omega1[1];
	hierarchy.qdot[2] = omega1[0];
}

void init_triple_comparison (ArticulatedHierarchy &hierarchy, ArticulatedHierarchy &hierarchy2) {
	init_pendulum_crash (hierarchy);
	init_pendulum_spherical (hierarchy2);

	// set them slightly apart
	hierarchy.model.X_T[1].r += Vector3d (0., 0., 0.5);
	hierarchy2.model.X_T[1].r += Vector3d (0., 0., -0.5);

	// Verify angular velocities:
	// RigidBodyDynamics::UpdateKinematicsCustom (hierarchy.model, &hierarchy.q, &hierarchy.qdot, NULL);

	// Vector3d w1 = hierarchy.model.v[3].block<3,1>(0,0);
	// Vector3d w2 = hierarchy.model.v[6].block<3,1>(0,0);
	// Vector3d w3 = hierarchy.model.v[9].block<3,1>(0,0);

	// cout << "hierarchy1: " << endl;
	// cout << "w1 = " << w1.transpose() << endl;
	// cout << "w2 = " << w2.transpose() << endl;
	// cout << "w3 = " << w3.transpose() << endl;

	// RigidBodyDynamics::UpdateKinematicsCustom (hierarchy2.model, &hierarchy2.q, &hierarchy2.qdot, NULL);
	// w1 = hierarchy2.model.v[1].block<3,1>(0,0);
	// w2 = hierarchy2.model.v[2].block<3,1>(0,0);
	// w3 = hierarchy2.model.v[3].block<3,1>(0,0);

	// cout << "hierarchy2: " << endl;
	// cout << "w1 = " << w1.transpose() << endl;
	// cout << "w2 = " << w2.transpose() << endl;
	// cout << "w3 = " << w3.transpose() << endl;
}

void init_floating_tree_hierarchy(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);

	// initialize model
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	unsigned int sphere1_id = hierarchy.addSphereBodyAtJoint (0, 0.5, 0.25, Vector3d (0., 3., 0.), spherical_zyx);
	unsigned int sphere2_id = hierarchy.addSphereBodyAtJoint (sphere1_id, 0.2, 0.15, Vector3d (0., -0.5, -1.), spherical_zyx);
	unsigned int sphere3_id = hierarchy.addSphereBodyAtJoint (sphere1_id, 0.1, 0.05, Vector3d (0., -0.5, 1.), spherical_zyx);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.q[0] = 0.3;
	hierarchy.q[3] = 0.6;
}

void init_chain_hierarchy(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);

	// initialize model
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	unsigned int sphere1_id = hierarchy.addSphereBodyAtJoint (0, 0.5, 0.15, Vector3d (0., 3.5, 0.), rot_z);
	unsigned int sphere2_id = hierarchy.addSphereBodyAtJoint (sphere1_id, 0.5, 0.15, Vector3d (0., -1., 0.), rot_x);
	unsigned int sphere3_id = hierarchy.addSphereBodyAtJoint (sphere2_id, 0.5, 0.15, Vector3d (0.,  0., 1.), rot_z);
	unsigned int sphere4_id = hierarchy.addSphereBodyAtJoint (sphere3_id, 0.5, 0.15, Vector3d (0., -1., 0.), rot_z);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.q[0] = 1.4;
	hierarchy.q[3] = 1.4;
}

void init_chain_hierarchy_contact (ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);

	// initialize mode
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	Joint joint_spherical (JointTypeSpherical);

	unsigned int sphere1_id = hierarchy.addSphereBodyAtJoint (0, 0.5, 0.15, Vector3d (0., 2.5, 0.), joint_spherical);
	unsigned int sphere2_id = hierarchy.addSphereBodyAtJoint (sphere1_id, 0.5, 0.15, Vector3d (0., -1., 0.), joint_spherical);
	unsigned int sphere3_id = hierarchy.addSphereBodyAtJoint (sphere2_id, 0.5, 0.15, Vector3d (0., -1., 0.), joint_spherical);
	unsigned int sphere4_id = hierarchy.addSphereBodyAtJoint (sphere3_id, 0.5, 0.15, Vector3d (0., -1., 0.), joint_spherical);

	hierarchy.initGeneralizedCoordinates();

	hierarchy.model.SetQuaternion (sphere1_id, Math::Quaternion::fromZYXAngles (Vector3d (1.4, 0., 1.4)), hierarchy.q);
	hierarchy.model.SetQuaternion (sphere2_id, Math::Quaternion::fromZYXAngles (Vector3d (0., 0., 0.)), hierarchy.q);
	hierarchy.model.SetQuaternion (sphere3_id, Math::Quaternion::fromZYXAngles (Vector3d (0., 0., 0.)), hierarchy.q);
	hierarchy.model.SetQuaternion (sphere4_id, Math::Quaternion::fromZYXAngles (Vector3d (0., 0., 0.)), hierarchy.q);
	
//////////////////	hierarchy.q[2] = 1.4;
}

void init_floating_chain_hierarchy(ArticulatedHierarchy &hierarchy) {
	// reset hierarchy
	hierarchy = ArticulatedHierarchy();

	hierarchy.plane = CollisionPlane (Vector3f (0.f, 1.f, 0.f), 0.f);

	// initialize model
	hierarchy.model.gravity = Vector3d (0., -9.81, 0.);

	unsigned int sphere1_id = hierarchy.addSphereBodyAtJoint (0, 0.5, 0.15, Vector3d (0., 0., 0.), floating_base);
	unsigned int sphere2_id = hierarchy.addSphereBodyAtJoint (sphere1_id, 0.5, 0.15, Vector3d (0., -1., 0.), spherical_zyx);
	unsigned int sphere3_id = hierarchy.addSphereBodyAtJoint (sphere2_id, 0.5, 0.15, Vector3d (0., -1., 0.), spherical_zyx);
	unsigned int sphere4_id = hierarchy.addSphereBodyAtJoint (sphere3_id, 0.5, 0.15, Vector3d (0., -1., 0.), spherical_zyx);

	hierarchy.initGeneralizedCoordinates();

//	hierarchy.q[0] = 0.3;
 	hierarchy.q[1] = 2.5;
	hierarchy.q[3] = 1.;
}

Simulation::Simulation() {

	activeScenarioId = 0;
	defaultRestitution = 0.3;
	defaultDamping = 0.0;
}

void Simulation::setRestitution(double value) {
	defaultRestitution = value;
	for (unsigned int i = 0; i < hierarchy.collisionSpheres.size(); i++) {
		hierarchy.collisionSpheres[i].restitution = defaultRestitution;
	}
}

void Simulation::reset() {
	cout << "Resetting simulation" << endl;

	time_accumulator = 0.;
	time_current = 0.;
	stepsize = 0.01;

	switch (activeScenarioId) {
		case 0:	init_simple_hierarchy_falling(hierarchy);
						break;
		case 1: init_simple_hierarchy_linear_velocity(hierarchy);
						break;
		case 2: init_simple_hierarchy_rotating(hierarchy);
						break;
		case 3: init_simple_hierarchy_rotating_lateral(hierarchy);
						break;
		case 4:	init_simple_hierarchy_sloped(hierarchy);
						break;
		case 5:	init_pendulum_crash(hierarchy);
						break;
		case 6:	init_pendulum_spherical(hierarchy);
						break;
		case 7: init_triple_comparison (hierarchy, hierarchy2);
						break;
		case 8:	init_chain_hierarchy(hierarchy);
						break;
		case 9:	init_chain_hierarchy_contact (hierarchy);
						break;
		case 10:	init_floating_chain_hierarchy(hierarchy);
						break;
		default:
						cerr << "Invalid scenario id!" << endl;
						abort();
	}

	setRestitution (defaultRestitution);
	hierarchy.velocityDampingFactor = defaultDamping;

	cout << "Resetting simulation done" << endl;
}

void Simulation::update(double timestep) {
	time_accumulator = time_accumulator + timestep;

	/*
	if (time_current == 0.) {
		cout << "# -------- Step 1" << endl;
		hierarchy.simulate (stepsize);
		cout << "# -------- Step 2" << endl;
		hierarchy.simulate (stepsize);
		cout << "# -------- Step 3" << endl;
		hierarchy.simulate (stepsize);
		cout << "# -------- Step 4" << endl;
		hierarchy.simulate (stepsize);
		cout << "# -------- Step 5" << endl;
		hierarchy.simulate (stepsize);

		time_current = stepsize * 5.;
	}

	return;
	*/

	while (time_accumulator - time_current > stepsize) {
		hierarchy.simulate (stepsize);
		if (activeScenarioId == 7) {
			hierarchy2.simulate (stepsize);
		}
		time_current = time_current + stepsize;

		double kinetic_energy = CalcKineticEnergy (hierarchy.model, hierarchy.q, hierarchy.qdot);
		double potential_energy = CalcPotentialEnergy (hierarchy.model, hierarchy.q);
		double energy = kinetic_energy + potential_energy;

		cout << "t = " << time_current << " energy = " << energy << " (kinetic: " << kinetic_energy << " potential: " << potential_energy << endl;
	}
}

void Simulation::draw(CommonGraphicsApp* app) {
	hierarchy.draw(app);
//	hierarchy.drawSpherePlaneContacts();

	if (activeScenarioId == 7) {
		hierarchy2.draw(app);
	}
}
