	
		project "App_rbdl_urdfreader"

		language "C++"
				
		kind "ConsoleApp"

  	includedirs {
                ".",
                "../../btgui",
                "../../vendor/rbdl/include",
                "../../vendor/rbdl/include/rbdl",
                }

			
		links{"urdf_tinyxml","rbdl-static"}
    
		files {
		"*.cc",
		"*.h",
		}
